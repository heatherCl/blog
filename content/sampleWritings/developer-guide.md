---
title: "CloudBees Core developer guide"
description: "Guide I created for CloudBees newest apps geared specifically towards creating a developer-centric workflow. I was the lone writer on this project."
tags: ["developer-centric"]
link: "https://docs.cloudbees.com/docs/cloudbees-core/latest/developer-guide/dev-guide-intro"
weight: 1
draft: false
---